//
//  MGDiskCacheTests.m
//  MGImageLibrary
//
//  Created by Maciek Gierszewski on 21.05.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "MGDiskCache.h"

@interface MGDiskCacheTests : XCTestCase
@property (nonatomic, strong) MGDiskCache *diskCache;
@end

@implementation MGDiskCacheTests

- (void)setUp
{
    [super setUp];
    
    self.diskCache = [[MGDiskCache alloc] initWithName:@"TestCache"];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSettingObject
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"item set"];
    
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"testImage" ofType:@"jpg"];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    
    [self.diskCache setObject:image forKey:path completion:^(BOOL success) {
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:3 handler:^(NSError *error) {
        XCTAssertNil(error);
        XCTAssertTrue([self.diskCache objectExistsForKey:path]);
    }];
}

- (void)testDeletingObject
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"item set"];
    
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"testImage" ofType:@"jpg"];
    
    [self.diskCache setObject:nil forKey:path completion:^(BOOL success) {
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:3 handler:^(NSError *error) {
        XCTAssertNil(error);
        XCTAssertFalse([self.diskCache objectExistsForKey:path]);
    }];
}

@end