//
//  MGDiskCache.h
//  MGImageLibrary
//
//  Created by Maciej Gierszewski on 18.06.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MGDiskCacheItemDescriptor.h"

extern NSInteger const MGDiskCacheDefaultCacheSize;

@interface MGDiskCache : NSObject
@property (nonatomic, assign) NSInteger cacheSize;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *directoryPath;

// init with default name
- (instancetype)init;

// init with cache name
- (instancetype)initWithName:(NSString *)cacheName;

// object getters / setters
- (id)objectForKey:(id)key;
- (BOOL)objectExistsForKey:(id)key;

- (void)setObject:(id)object forKey:(id)aKey completion:(void (^)(BOOL success))completionBlock;
@end
