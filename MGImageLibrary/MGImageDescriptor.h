//
//  MGImageDescriptor.h
//  MGImageLibrary
//
//  Created by Maciej Gierszewski on 18.06.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ImageDescriptor(image,path) ([MGImageDescriptor imageDescriptorWithImage:(image) sourcePath:(path)])

@interface MGImageDescriptor : NSObject
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *sourcePath;

+ (MGImageDescriptor *)imageDescriptorWithImage:(UIImage *)image sourcePath:(NSString *)path;
@end