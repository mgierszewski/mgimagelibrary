//
//  MGDiskCache.m
//  MGImageLibrary
//
//  Created by Maciej Gierszewski on 18.06.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGDiskCache.h"
#import "NSData+md5.h"
#import "UIImage+AnimatedGIF.h"

NSInteger const MGDiskCacheDefaultCacheSize = 50;

@interface MGDiskCache ()
@property (nonatomic, strong) NSString *directoryPath;
@property (nonatomic, strong) NSMutableArray *directoryContents;

@property (nonatomic, strong) NSOperationQueue *trimQueue;
@property (nonatomic, strong) NSOperationQueue *fileQueue;

- (NSString *)pathForKey:(id)key;
- (MGDiskCacheItemDescriptor *)itemDescriptorForItemAtPath:(NSString *)path;

- (void)deleteItemWithDescriptior:(MGDiskCacheItemDescriptor *)descriptor;
- (void)addItemWithData:(NSData *)itemData withPath:(NSString *)path;
- (void)directoryCleanup;

@end

@implementation MGDiskCache

- (instancetype)init
{
    return [self initWithName:@"MGDiskCache"];
}

- (instancetype)initWithName:(NSString *)cacheName
{
    self = [super init];
    if (self)
    {
        NSURL* cachesDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
        
        self.cacheSize = MGDiskCacheDefaultCacheSize;
        self.directoryContents = [NSMutableArray array];
        self.directoryPath = [cachesDirectory.path stringByAppendingPathComponent:cacheName];
        
        self.fileQueue = [NSOperationQueue new];
        
        self.trimQueue = [NSOperationQueue new];
        self.trimQueue.maxConcurrentOperationCount = 1;
        
        // create directory if needed
        BOOL isDirectory;
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:self.directoryPath isDirectory:&isDirectory];
        if (!fileExists || !isDirectory)
        {
            NSError *createDirectoryError = nil;
            [[NSFileManager defaultManager] createDirectoryAtPath:self.directoryPath withIntermediateDirectories:NO attributes:nil error:&createDirectoryError];
            
            if (createDirectoryError)
            {
                NSLog(@"%@", createDirectoryError);
            }
        }
        
        // read the contents of the dictionary
        NSError *contentsError = nil;
        NSArray *contentsArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:self.directoryPath error:&contentsError];

        if (contentsError)
        {
            NSLog(@"%@", contentsError);
        }
        
        NSArray *extensions = @[@"jpg", @"png", @"gif"];
        [contentsArray enumerateObjectsUsingBlock:^(NSString *itemName, NSUInteger idx, BOOL *stop) {
            
            if ([extensions containsObject:itemName.pathExtension.lowercaseString])
            {
                NSString *itemPath = [self.directoryPath stringByAppendingPathComponent:itemName];
               
                NSError *attributesError = nil;
                NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:itemPath error:&attributesError];
                if (attributesError)
                {
                    NSLog(@"%@", attributesError);
                }
                else
                {
                    NSDate *creationDate = attributes[NSFileCreationDate];
                    MGDiskCacheItemDescriptor *itemDescriptor = [MGDiskCacheItemDescriptor diskCacheItemDescriptorWithPath:itemPath creationDate:creationDate];
                    
                    [self.directoryContents addObject:itemDescriptor];
                }
            }
        }];
        
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES];
        [self.directoryContents sortUsingDescriptors:@[sortDescriptor]];
        
        // keep the cache size
        [self directoryCleanup];
    }
    return self;
}

#pragma mark - Getters

- (BOOL)objectExistsForKey:(id)aKey
{
    NSString *path = [self pathForKey:aKey];
    NSString *itemPath = [self.directoryPath stringByAppendingPathComponent:path ?: @""];
    
    return [[NSFileManager defaultManager] fileExistsAtPath:itemPath];
}

- (id)objectForKey:(id)aKey
{
    NSString *path = [self pathForKey:aKey];
    
    // read data
    NSString *itemPath = [self.directoryPath stringByAppendingPathComponent:path ?: @""];
    NSData *itemData = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:itemPath])
    {
        itemData = [[NSData alloc] initWithContentsOfFile:itemPath];
		
		NSInteger directoryItemIndex = [self.directoryContents indexOfObjectPassingTest:^BOOL(MGDiskCacheItemDescriptor *descriptor, NSUInteger idx, BOOL *stop) {
			return [descriptor.path isEqual:itemPath];
		}];
		
		// move the descriptor to the end of the directory contents list (updating the date)
		if (directoryItemIndex != NSNotFound)
		{
			MGDiskCacheItemDescriptor *descriptor = self.directoryContents[directoryItemIndex];
//			descriptor.creationDate = [NSDate date]; // is it necessary?
			
			[self.directoryContents removeObject:descriptor];
			[self.directoryContents addObject:descriptor];
		}
    }
    
    return itemData;
}

#pragma mark - Setters

- (void)setCacheSize:(NSInteger)cacheSize
{
    _cacheSize = cacheSize;
    [self directoryCleanup];
}

- (void)setObject:(id)object forKey:(id)aKey completion:(void (^)(BOOL success))completionBlock
{
    [self.fileQueue addOperationWithBlock:^{
        
        NSString *cachedItemName = [self pathForKey:aKey];
        if (!cachedItemName)
        {
            if (completionBlock)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock(NO);
                });
            }
            return;
        }
        
        // existing item descriptors (in case of overwrite or delete)
        if (!object)
        {
            NSString *itemPath = [self.directoryPath stringByAppendingPathComponent:cachedItemName];
            MGDiskCacheItemDescriptor *itemDescriptor = [self itemDescriptorForItemAtPath:itemPath];
            [self deleteItemWithDescriptior:itemDescriptor];
            
            if (completionBlock)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock(NO);
                });
            }
            return;
        }
        
        // save data to the path
        NSData *itemData = nil;
        if ([object isKindOfClass:[UIImage class]])
        {
            NSString *pathExtension = [aKey isKindOfClass:[NSString class]] ? [[(NSString *)aKey pathExtension] lowercaseString] : nil;
            if ([pathExtension rangeOfString:@"jpg"].location != NSNotFound || [pathExtension rangeOfString:@"jpeg"].location != NSNotFound)
            {
                itemData = UIImageJPEGRepresentation(object, 1.0f);
            }
            else if ([pathExtension rangeOfString:@"png"].location != NSNotFound)
            {
                itemData = UIImagePNGRepresentation(object);
            }
            else if ([pathExtension rangeOfString:@"gif"].location != NSNotFound)
            {
                itemData = UIImageAnimatedGIFRepresentation(object, 0.0, 0);
            }
        }
        else if ([object isKindOfClass:[NSString class]])
        {
            itemData = [(NSString *)object dataUsingEncoding:NSUTF8StringEncoding];
        }
        else if ([object isKindOfClass:[NSData class]])
        {
            itemData = object;
        }
        
        if (!itemData)
        {
            if (completionBlock)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock(NO);
                });
            }
            return;
        }
        
        [self addItemWithData:itemData withPath:cachedItemName];
        if (completionBlock)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(YES);
            });
        }

        // keep the cache size
        [self directoryCleanup];
    }];
}

#pragma mark - Core

- (void)deleteItemWithDescriptior:(MGDiskCacheItemDescriptor *)descriptor
{
    if (!descriptor)
    {
        return;
    }
    
    @synchronized(self.directoryContents)
    {
        // delete object from cache (if exists)
        if ([[NSFileManager defaultManager] fileExistsAtPath:descriptor.path])
        {
            NSError *removeError = nil;
            [[NSFileManager defaultManager] removeItemAtPath:descriptor.path error:&removeError];
        }
        
        [self.directoryContents removeObject:descriptor];
    }
}

- (MGDiskCacheItemDescriptor *)itemDescriptorForItemAtPath:(NSString *)path
{
    if (!path)
    {
        return nil;
    }
    
    MGDiskCacheItemDescriptor *itemDescriptor = nil;
    @synchronized(self.directoryContents)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"path = %@", path];
        @try {
            itemDescriptor = [[self.directoryContents filteredArrayUsingPredicate:predicate] firstObject];
        }
        @catch (NSException *exception) {
        }
    }
    return itemDescriptor;
}

- (void)addItemWithData:(NSData *)itemData withPath:(NSString *)path
{
    if (!itemData || !path)
    {
        return;
    }
    
    // get the item descriptor
    MGDiskCacheItemDescriptor *itemDescriptor = [self itemDescriptorForItemAtPath:path];
    @synchronized(self.directoryContents)
    {
        NSString *itemPath = [self.directoryPath stringByAppendingPathComponent:path];
        [itemData writeToFile:itemPath atomically:YES];
        
        // update or create the item descriptor
        if (itemDescriptor)
        {
            itemDescriptor.creationDate = [NSDate date];
        }
        else
        {
            MGDiskCacheItemDescriptor *itemDescriptor = [MGDiskCacheItemDescriptor diskCacheItemDescriptorWithPath:itemPath creationDate:[NSDate date]];
            [self.directoryContents addObject:itemDescriptor];
        }
    }
}

- (void)directoryCleanup
{
    [self.trimQueue addOperationWithBlock:^{
        
        while ([self.directoryContents count] > self.cacheSize)
        {
            MGDiskCacheItemDescriptor *oldestItemDescriptor = [self.directoryContents firstObject];
            [self deleteItemWithDescriptior:oldestItemDescriptor];
        }
    }];
}

- (NSString *)pathForKey:(id)key
{
    NSData *keyData = nil;
    NSString *extension = nil;
    
    if ([key isKindOfClass:[NSString class]])
    {
        keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
        extension = [(NSString *)key pathExtension];
    }
    else if ([key isKindOfClass:[NSURL class]])
    {
        keyData = [[(NSURL *)key absoluteString] dataUsingEncoding:NSUTF8StringEncoding];
    }
    else if ([key isKindOfClass:[NSData class]])
    {
        keyData = key;
    }
    else
    {
        NSString *keyString = [NSString stringWithFormat:@"%@", key];
        keyData = [keyString dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    NSString *path = [keyData md5];
    if (extension)
    {
        path = [path stringByAppendingPathExtension:extension];
    }
    return path;
}

@end
