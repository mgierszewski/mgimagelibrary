//
//  MGImageDescriptor.m
//  MGImageLibrary
//
//  Created by Maciej Gierszewski on 18.06.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGImageDescriptor.h"

@implementation MGImageDescriptor

+ (MGImageDescriptor *)imageDescriptorWithImage:(UIImage *)image sourcePath:(NSString *)path
{
    MGImageDescriptor *descriptor = [MGImageDescriptor new];
    descriptor.image = image;
    descriptor.sourcePath = path;
    
    return descriptor;
}

@end
