#import "UIImage+AnimatedGIF.h"
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <UIKit/UIKit.h>

@implementation UIImage (AnimatedGIF)

+ (UIImage *)imageWithAnimatedGIFData:(NSData *)data
{
    return [UIImage imageWithAnimatedGIFData:data scale:[[UIScreen mainScreen] scale] duration:0.0];
}

+ (UIImage *)imageWithAnimatedGIFData:(NSData *)data scale:(CGFloat)scale duration:(NSTimeInterval)duration
{
    if (!data)
    {
        return nil;
    }
    
    // image source options
    NSDictionary *options = @{
                              (NSString *)kCGImageSourceShouldCache: @YES,
                              (NSString *)kCGImageSourceTypeIdentifierHint: (NSString *)kUTTypeGIF
                              };
    
    CGImageSourceRef imageSource = CGImageSourceCreateWithData((__bridge CFDataRef)data, (__bridge CFDictionaryRef)options);
    
    // number of frames and images container
    size_t numberOfFrames = CGImageSourceGetCount(imageSource);
    NSMutableArray *mutableImages = [NSMutableArray arrayWithCapacity:numberOfFrames];
    
    // get frames and their duration
    NSTimeInterval calculatedDuration = 0.0f;
    for (size_t i = 0; i < numberOfFrames; i++)
    {
        NSDictionary *properties = (__bridge_transfer NSDictionary *)CGImageSourceCopyPropertiesAtIndex(imageSource, i, 0);
        NSDictionary *gifProperties = properties[(__bridge NSString*)kCGImagePropertyGIFDictionary];
        
        NSTimeInterval frameDuration = [gifProperties[(__bridge NSString*)kCGImagePropertyGIFDelayTime] doubleValue];
        NSTimeInterval unclampedframeDuration = [gifProperties[(__bridge NSString*)kCGImagePropertyGIFUnclampedDelayTime] doubleValue];

        calculatedDuration += (frameDuration > 0.0 ? frameDuration : (unclampedframeDuration > 0.0 ? unclampedframeDuration : 0.1));
        
        CGImageRef imageRef = CGImageSourceCreateImageAtIndex(imageSource, i, (__bridge CFDictionaryRef)options);
        [mutableImages addObject:[UIImage imageWithCGImage:imageRef scale:scale orientation:UIImageOrientationUp]];
        
        CGImageRelease(imageRef);
    }
    CFRelease(imageSource);
    
    if (numberOfFrames == 1)
    {
        return [mutableImages firstObject];
    }

    return [UIImage animatedImageWithImages:mutableImages duration:(duration <= 0.0 ? calculatedDuration : duration)];
}

NSData * UIImageAnimatedGIFRepresentation(UIImage *image, NSTimeInterval duration, NSUInteger loopCount)
{
    if (!image.images)
    {
        return nil;
    }
    
    // frame count and duration
    size_t frameCount = [image.images count];
    float frameDuration = duration <= 0.0 ? (float)image.duration / frameCount : (float)duration / frameCount;
    NSDictionary *frameProperties = @{
                                      (__bridge NSString*)kCGImagePropertyGIFDictionary: @{
                                              (__bridge NSString*)kCGImagePropertyGIFDelayTime: @(frameDuration)
                                              }
                                      };
    
    // data container
    NSMutableData *mutableData = [NSMutableData data];
    CGImageDestinationRef destination = CGImageDestinationCreateWithData((__bridge CFMutableDataRef)mutableData, kUTTypeGIF, frameCount, 0);
    
    NSDictionary *imageProperties = @{
                                      (__bridge NSString*) kCGImagePropertyGIFDictionary: @{
                                              (__bridge NSString*) kCGImagePropertyGIFLoopCount: @(loopCount),
                                              }
                                      };
    CGImageDestinationSetProperties(destination, (__bridge CFDictionaryRef)imageProperties);
    
    // add frames to the data container
    for (size_t i = 0; i < [image.images count]; i++)
    {
        CGImageDestinationAddImage(destination, [image.images[i] CGImage], (__bridge CFDictionaryRef)frameProperties);
    }
    
    BOOL success = CGImageDestinationFinalize(destination);
    CFRelease(destination);
    
    if (!success)
    {
        return nil;
    }
    
    return [NSData dataWithData:mutableData];
}

@end
