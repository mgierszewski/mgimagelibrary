#import <UIKit/UIKit.h>

@interface UIImage (AnimatedGIF)
+ (UIImage *)imageWithAnimatedGIFData:(NSData *)data;
+ (UIImage *)imageWithAnimatedGIFData:(NSData *)data scale:(CGFloat)scale duration:(NSTimeInterval)duration;
@end

extern NSData * UIImageAnimatedGIFRepresentation(UIImage *image, NSTimeInterval duration, NSUInteger loopCount);
