//
//  MGDiskCacheItem.m
//  MGImageLibrary
//
//  Created by Maciej Gierszewski on 18.06.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGDiskCacheItemDescriptor.h"

@implementation MGDiskCacheItemDescriptor

+ (MGDiskCacheItemDescriptor *)diskCacheItemDescriptorWithPath:(NSString *)path creationDate:(NSDate *)date
{
    MGDiskCacheItemDescriptor *descriptor = [MGDiskCacheItemDescriptor new];
    descriptor.path = path;
    descriptor.creationDate = date;
    
    return descriptor;
}

@end
