//
//  MGCache.h
//  MGImageLibrary
//
//  Created by Maciek Gierszewski on 20.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MGCache;
@protocol MGCacheDelegate <NSObject>
- (void)cache:(MGCache *)cache willRemoveObject:(id)object;
@end

@interface MGCache : NSObject
@property (weak) id<MGCacheDelegate> delegate;

- (id)objectForKey:(id)key;

- (void)setObject:(id)obj forKey:(id)key; // 0 cost
- (void)setObject:(id)obj forKey:(id)key cost:(NSUInteger)cost;

- (void)removeObjectForKey:(id)key;
- (void)removeAllObjects;

@property NSUInteger totalCostLimit;
@end
