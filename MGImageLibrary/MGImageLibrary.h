//
//  MGImageLibrary.h
//  MGImageLibrary
//
//  Created by Maciej Gierszewski on 18.06.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MGImageDescriptor.h"

typedef void (^MGImageLibraryCompletionBlock)(MGImageDescriptor *imageDescriptor, NSError *error);

@interface MGImageLibrary : NSObject
@property (nonatomic, copy) NSDictionary *requestHeaders;

@property (nonatomic, assign) NSTimeInterval timeoutInterval;
@property (nonatomic, assign) NSInteger maximumDownloadOperationCount;
@property (nonatomic, assign) NSInteger level2CacheItemCount;
@property (nonatomic, assign) NSInteger level1CacheSize;
@property (nonatomic, assign) BOOL useLevel1Cache;
@property (nonatomic, assign) BOOL useLevel2Cache;
@property (nonatomic, assign) BOOL returnImagesWithFixedOrientation;

+ (MGImageLibrary *)sharedInstance;

- (MGImageDescriptor *)cachedImageFromPath:(NSString *)path;
- (void)saveImageToDiskCache:(UIImage *)image completionBlock:(MGImageLibraryCompletionBlock)block;

- (void)loadImageFromPath:(NSString *)path completionBlock:(MGImageLibraryCompletionBlock)block;
@end