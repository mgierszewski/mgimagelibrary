//
//  MGImageLibrary.m
//  MGImageLibrary
//
//  Created by Maciej Gierszewski on 18.06.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGImageLibrary.h"
#import "MGDiskCache.h"
#import "MGCache.h"
#import "NSData+md5.h"
#import "UIImage+AnimatedGIF.h"
#import "UIImage+Orientation.h"

#ifdef DEBUG
#define LOG_TIME 1
#endif

#define USE_NSCACHE 1
#ifdef USE_NSCACHE
#define CacheClass NSCache
#else
#define CacheClass MGCache
#endif

@interface MGImageLibrary () // <MGCacheDelegate, NSCacheDelegate>
@property (nonatomic, getter = isInitialized) BOOL initialized;
@property (nonatomic, strong) NSMutableDictionary *notifyTargetsDictionary;

@property (nonatomic, strong) NSOperationQueue *downloadQueue;
@property (nonatomic, strong) NSOperationQueue *cacheQueue;

@property (nonatomic, strong) CacheClass *cache;
@property (nonatomic, strong) MGDiskCache *diskCache;

- (MGImageDescriptor *)imageWithData:(NSData *)data sourcePath:(NSString *)sourcePath;

- (BOOL)imageExistsInCache:(NSString *)path;
@end

@implementation MGImageLibrary

+ (MGImageLibrary *)sharedInstance
{
    static MGImageLibrary *MGImageLibraryInstance = nil;
    static dispatch_once_t MGImageLibraryOnceToken;
    dispatch_once(&MGImageLibraryOnceToken, ^{
        MGImageLibraryInstance = [[super allocWithZone:0] init];
    });

    return MGImageLibraryInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
	return [MGImageLibrary sharedInstance];
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        if (!self.isInitialized)
        {
            self.timeoutInterval = 15.0;
            self.maximumDownloadOperationCount = NSOperationQueueDefaultMaxConcurrentOperationCount;
            
            self.useLevel1Cache = YES;
            self.useLevel2Cache = YES;
            self.returnImagesWithFixedOrientation = YES;
            
            // image cache
            self.cache = [CacheClass new];
            self.cache.totalCostLimit = 5 * 1000 * 1000; // 50 MB
        
            // disk cache
            self.diskCache = [[MGDiskCache alloc] initWithName:@"MGDiskCache"];
        
            // download queue
            self.downloadQueue = [NSOperationQueue new];
            
            // cache queue
            self.cacheQueue = [NSOperationQueue new];
        
            // targets to notify when the image download completes
            self.notifyTargetsDictionary = [NSMutableDictionary new];
            
            // handle app states and memory warnings
            [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification
                                                              object:nil
                                                               queue:[NSOperationQueue mainQueue]
                                                          usingBlock:^(NSNotification *note) {
                                                              [self.cache removeAllObjects];
                                                          }];
            [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillTerminateNotification
                                                              object:nil
                                                               queue:[NSOperationQueue mainQueue]
                                                          usingBlock:^(NSNotification *note) {
                                                              [self.cache removeAllObjects];
                                                          }];
            [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidEnterBackgroundNotification
                                                              object:nil
                                                               queue:[NSOperationQueue mainQueue]
                                                          usingBlock:^(NSNotification *note) {
                                                              [self.cache removeAllObjects];
                                                          }];
            
            self.initialized = YES;
        }
    }
    return self;
}

- (void)dealloc
{
    [self.cache removeAllObjects];
}

#pragma mark - Image Management

- (BOOL)imageExistsInCache:(NSString *)path
{
    if ([self.cache objectForKey:path])
    {
        return YES;
    }
    
    if ([self.diskCache objectExistsForKey:path])
    {
        return YES;
    }
    
    return NO;
}

- (MGImageDescriptor *)imageWithData:(NSData *)data sourcePath:(NSString *)sourcePath
{
    UIImage *image = nil;
    NSString *pathExtension = [sourcePath.pathExtension lowercaseString];
    if ([pathExtension rangeOfString:@"gif"].location != NSNotFound)
    {
        image = [UIImage imageWithAnimatedGIFData:data];
    }
    else
    {
        image = [UIImage imageWithData:data];
        if (self.returnImagesWithFixedOrientation)
        {
            image = [image imageWithFixedOrientation];
        }
    }
    MGImageDescriptor *imageDescriptor = ImageDescriptor(image, sourcePath);
    
    return imageDescriptor;
}

- (MGImageDescriptor *)cachedImageFromPath:(NSString *)path
{
    // check lvl 1 cache
    MGImageDescriptor *cachedImageDescriptor = [self.cache objectForKey:path];
    if (cachedImageDescriptor)
    {
        return cachedImageDescriptor;
    }
    
    // check lvl 2 cache
    NSData *cachedImageData = [self.diskCache objectForKey:path];
    if (cachedImageData)
    {
        MGImageDescriptor *imageDescriptor = [self imageWithData:cachedImageData sourcePath:path];

        // save image back to the lvl 1 cache
        if (self.useLevel1Cache)
        {
            [self.cache setObject:imageDescriptor forKey:path cost:cachedImageData.length];
        }
       
        return imageDescriptor;
    }
    
    return nil;
}

- (void)loadImageFromPath:(NSString *)path completionBlock:(MGImageLibraryCompletionBlock)block
{
    if (block == nil)
    {
        return;
    }
    
    if (path == nil)
    {
        block(nil, nil);
        return;
    }
    
    // check cache
    if ([self imageExistsInCache:path])
    {
        MGImageDescriptor *cachedImageDescriptor = [self cachedImageFromPath:path];
        block(cachedImageDescriptor, nil);

        return;
    }
    
    //
    @synchronized(self.notifyTargetsDictionary)
    {
        NSMutableArray *notifyTargets = self.notifyTargetsDictionary[path];
        if (notifyTargets)
        {
            // if notify targets queue exists, add block to that queue
            [notifyTargets addObject:block];
            return;
        }
        else
        {
            // create new queue, proceed
            notifyTargets = [NSMutableArray arrayWithObject:block];
            self.notifyTargetsDictionary[path] = notifyTargets;
        }
    }
    
    NSBlockOperation *downloadOperation = [NSBlockOperation new];
    __weak NSBlockOperation *weakDownloadOperation = downloadOperation;
    
    [downloadOperation addExecutionBlock:^{
        
        NSURL *url = [NSURL URLWithString:path];
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:self.timeoutInterval];
        
        [self.requestHeaders enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [urlRequest addValue:obj forHTTPHeaderField:key];
        }];
        
        __block NSURLResponse *urlResponse;
        __block NSError *connectionError = nil;
        __block NSData *receivedData;

        #if LOG_TIME == 1
        CFAbsoluteTime downloadStart = CFAbsoluteTimeGetCurrent();
        #endif

        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

        [[[NSURLSession sharedSession] dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            urlResponse = response;
            receivedData = data;
            connectionError = error;
            
            dispatch_semaphore_signal(semaphore);
        }] resume];
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);

        #if LOG_TIME == 1
        CFAbsoluteTime downloadEnd = CFAbsoluteTimeGetCurrent();
        #endif
        
        if (!weakDownloadOperation.isCancelled)
        {
            if (connectionError)
            {
                // notify targets about the error
                @synchronized(self.notifyTargetsDictionary)
                {
                    NSMutableArray *notifyTargets = self.notifyTargetsDictionary[path];
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        [notifyTargets enumerateObjectsUsingBlock:^(MGImageLibraryCompletionBlock block, NSUInteger idx, BOOL *stop) {
                            block(nil, connectionError);
                        }];
                    }];
                    [self.notifyTargetsDictionary removeObjectForKey:path];
                }
                return;
            }

            [self.cacheQueue addOperationWithBlock:^{
                
                // create image from data
                #if LOG_TIME == 1
                CFAbsoluteTime createStart = CFAbsoluteTimeGetCurrent();
                #endif
                
                MGImageDescriptor *imageDescriptor = [self imageWithData:receivedData sourcePath:path];
                
                #if LOG_TIME == 1
                CFAbsoluteTime createEnd = CFAbsoluteTimeGetCurrent();
                #endif
                
                #if LOG_TIME == 1
                NSLog(@"%@: download: %fs, data length: %lu, create: %fs", path, downloadEnd - downloadStart, (unsigned long)receivedData.length, createEnd - createStart);
                #endif
                
                // save to cache
                if (self.useLevel1Cache && [[path lowercaseString] rangeOfString:@"gif"].location == NSNotFound)
                {
                    [self.cache setObject:imageDescriptor forKey:path cost:receivedData.length];
                }
                
                if (self.useLevel2Cache)
                {
                    [self.diskCache setObject:imageDescriptor.image forKey:path completion:nil];
                }
                
                // notify targets
                @synchronized(self.notifyTargetsDictionary)
                {
                    NSMutableArray *notifyTargets = self.notifyTargetsDictionary[path];
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        
                        [notifyTargets enumerateObjectsUsingBlock:^(MGImageLibraryCompletionBlock block, NSUInteger idx, BOOL *stop) {
                            block(imageDescriptor, nil);
                        }];
                    }];
                    [self.notifyTargetsDictionary removeObjectForKey:path];
                }
            }];
        }
    }];
    
    [self.downloadQueue addOperation:downloadOperation];
}

- (void)saveImageToDiskCache:(UIImage *)image completionBlock:(MGImageLibraryCompletionBlock)block
{
    if (!image)
    {
        if (block)
        {
            block(nil, nil);
        }
        return;
    }
    
    if (!self.useLevel2Cache)
    {
        return;
    }
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *imageName = [NSString stringWithFormat:@"%@.png", [imageData md5]];
    NSString *imageLocation = [self.diskCache.directoryPath stringByAppendingPathComponent:imageName];
    NSString *path = [[NSURL fileURLWithPath:imageLocation isDirectory:NO] absoluteString];

    [self.diskCache setObject:image forKey:path completion:^(BOOL success) {
        if (block)
        {
            MGImageDescriptor *imageDescriptor = success ? [MGImageDescriptor imageDescriptorWithImage:image sourcePath:path] : nil;
            block(imageDescriptor, nil);
        }
    }];
}

#pragma mark - Setters

- (void)setMaximumDownloadOperationCount:(NSInteger)maximumDownloadOperationCount
{
    _maximumDownloadOperationCount = maximumDownloadOperationCount;
    self.downloadQueue.maxConcurrentOperationCount = _maximumDownloadOperationCount;
}

- (void)setLevel2CacheItemCount:(NSInteger)level2CacheItemCount
{
    self.diskCache.cacheSize = level2CacheItemCount;
}

- (void)setLevel1CacheSize:(NSInteger)level1CacheSize
{
    self.cache.totalCostLimit = level1CacheSize;
}

#pragma mark - Getters

- (NSInteger)level2CacheItemCount
{
    return self.diskCache.cacheSize;
}

- (NSInteger)level1CacheSize
{
    return self.cache.totalCostLimit;
}

//#pragma mark - MGCacheDelegate

//- (void)cache:(NSCache *)cache willEvictObject:(id)object
//{
//    MGImageDescriptor *imageDescriptor = (MGImageDescriptor *)object;
//    if (self.useLevel2Cache)
//    {
//        [self.diskCache setObject:imageDescriptor.image forKey:imageDescriptor.sourcePath];
//    }
//}
//
//- (void)cache:(MGCache *)cache willRemoveObject:(id)object
//{
//    MGImageDescriptor *imageDescriptor = (MGImageDescriptor *)object;
//    if (self.useLevel2Cache)
//    {
//        [self.diskCache setObject:imageDescriptor.image forKey:imageDescriptor.sourcePath];
//    }
//}

@end
