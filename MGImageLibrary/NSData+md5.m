//
//  NSString+MG.m
//  MGAlertView
//
//  Created by Maciej Gierszewski on 05.11.2013.
//  Copyright (c) 2013 Future Mind Sp. z o. o. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>
#import "NSData+md5.h"

@implementation NSData (md5)

- (NSString *)md5
{
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    if (CC_MD5(self.bytes, (int)self.length, digest))
    {
        for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        {
            [result appendFormat:@"%02x", digest[i]];
        }
    }
    
    return result;
}

@end
