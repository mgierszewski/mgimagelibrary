//
//  MGDiskCacheItem.h
//  MGImageLibrary
//
//  Created by Maciej Gierszewski on 18.06.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MGDiskCacheItemDescriptor : NSObject
@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSDate *creationDate;

+ (MGDiskCacheItemDescriptor *)diskCacheItemDescriptorWithPath:(NSString *)path creationDate:(NSDate *)date;
@end
