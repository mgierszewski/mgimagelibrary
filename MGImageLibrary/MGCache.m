//
//  MGCache.m
//  MGImageLibrary
//
//  Created by Maciek Gierszewski on 20.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import "MGCache.h"

@interface MGCache ()
@property (nonatomic, strong) NSMutableDictionary *objectDictionary;
@property (nonatomic, strong) NSMutableDictionary *costDictionary;

@property (nonatomic, assign) NSUInteger totalCost;
@end

@implementation MGCache

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.objectDictionary = [NSMutableDictionary new];
        self.costDictionary = [NSMutableDictionary new];
        self.totalCost = 0;
        self.totalCostLimit = 5 * 1000 * 1000;
    }
    return self;
}

- (void)setObject:(id)obj forKey:(id)key
{
    [self setObject:obj forKey:key cost:0];
}

- (void)setObject:(id)obj forKey:(id)key cost:(NSUInteger)cost
{
    @synchronized(self)
    {
        [self.objectDictionary setObject:obj forKey:key];
        [self.costDictionary setObject:@(cost) forKey:key];
        
        self.totalCost += cost;
    }
    
    while (self.totalCost >= self.totalCostLimit)
    {
        NSArray *keys = [self.costDictionary keysSortedByValueUsingSelector:@selector(compare:)];
        id highestCostKey = [keys lastObject];
        
        [self removeObjectForKey:highestCostKey];
    }
}

- (void)removeObjectForKey:(id)key
{
    id obj = [self objectForKey:key];
    if (!obj)
    {
        return;
    }
    
    @synchronized(self)
    {
        if ([self.delegate respondsToSelector:@selector(cache:willRemoveObject:)])
        {
            [self.delegate cache:self willRemoveObject:obj];
        }
    
        NSUInteger cost = [self.costDictionary[key] unsignedIntegerValue];
        self.totalCost -= cost;
        
        [self.objectDictionary removeObjectForKey:key];
        [self.costDictionary removeObjectForKey:key];
    }
}

- (void)removeAllObjects
{
    NSArray *keys = [self.objectDictionary allKeys];
    for (id key in keys)
    {
        [self removeObjectForKey:key];
    }
}

- (id)objectForKey:(id)key
{
    @synchronized(self)
    {
        return [self.objectDictionary objectForKey:key];
    }
}

@end
