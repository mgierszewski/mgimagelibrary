//
//  UIImage+Orientation.h
//  MGImageLibrary
//
//  Created by Maciek Gierszewski on 16.04.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Orientation)
- (UIImage *)imageWithFixedOrientation;
@end
