//
//  NSData+MG.h
//  MGAlertView
//
//  Created by Maciej Gierszewski on 05.11.2013.
//  Copyright (c) 2013 Future Mind Sp. z o. o. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (md5)
@property (nonatomic, readonly) NSString *md5;
@end
